# Infrastructure

## GCP - GKE

[gcloud auth login](https://cloud.google.com/sdk/gcloud/reference/auth/login)
[Quickstart](https://cloud.google.com/kubernetes-engine/docs/quickstart)

## Add GKE cluster to our group

Follow [this guide](https://docs.gitlab.com/ee/user/project/clusters/add_gke_clusters.html#creating-the-cluster-on-gke)

## K8S : Use Gitlab Container registry as images repository

* Generate Gitlab group Token: <https://docs.gitlab.com/ee/user/project/deploy_tokens/#read-container-registry-images>
* Run: kubectl create secret docker-registry gitlab-registry-secret --docker-server=registry.gitlab.com --docker-username=kubernetes-registry-token --docker-password=$PASSWORD
* Add use: 
```
imagePullSecrets:
  - name: gitlab-registry-secret
```

## Terraform import GKE state

* ```terraform import google_container_cluster.minekloud-euw1-c projects/minekloud-k8s/locations/europe-west1-c/clusters/minekloud-euw1-c```
* ```terraform import google_container_node_pool.primary_preemptible_nodes minekloud-k8s/europe-west1-c/minekloud-euw1-c/minekloud-node-pool-1```

Some helpfull cmd :

```bash
terraform init \
  -backend-config=address=https://gitlab.com/api/v4/projects/22012942/terraform/state/production \
  -backend-config=lock_address=https://gitlab.com/api/v4/projects/22012942/terraform/state/production/lock \
  -backend-config=unlock_address=https://gitlab.com/api/v4/projects/22012942/terraform/state/production/lock \
  -backend-config=username=terraform-debug-token \
  -backend-config=password=<PASSWORD-TOKEN> \
  -backend-config=lock_method=POST \
  -backend-config=unlock_method=DELETE \
  -backend-config=retry_wait_min=5
```

```bash
curl --header "Private-Token: <PASSWORD-TOKEN>" --request DELETE "https://gitlab.example.com/api/v4/projects/22012942/terraform/state/production"
```